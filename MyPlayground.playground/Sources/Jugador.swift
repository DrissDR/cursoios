import Foundation

class Jugador: NSObject {
    var credito = 100
    var arruinado = false
    var alcohol: Int
    
    override init() {
        self.alcohol = 0
    }
    
    init(alcohol: Int) {
        self.alcohol = alcohol
    }
    
    init(alcohol: Int, credito:Int) {
        self.alcohol = alcohol
        self.credito = credito
    }
    
   
    
    func pierdeCredito(cantidad:Int) {
        credito -= cantidad
        if credito <= 0 {
            jugadorArruinado()
        }
        print("Credito actual es :::: \(credito)")
    }
    
    func jugadorArruinado() {
        arruinado = true
        print("ARRUINADO")
    }
    
}

let juan:Jugador = Jugador()